package com.dev_learning.ios_test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;



public class Test {
	
	IOSDriver<IOSElement> driver;
	
	public Test() throws MalformedURLException{
		DesiredCapabilities capabilities = new DesiredCapabilities();
		//capabilities.setBrowserName("IPHONE 5S");
		capabilities.setCapability("deviceName", "iPhone 5s"); //A valid device name ( Emulator booted )
		capabilities.setCapability("platformVersion", "10.2"); //IOS Version
		capabilities.setCapability("platformName", "iOS");
		capabilities.setCapability("udid", "2ff327fc4519f7622dd68dbaf6233aaf680d3bdb");
		capabilities.setCapability("newCommandTimeout",300);
		capabilities.setCapability("noReset",true);
		capabilities.setCapability("automationName", "XCUITest");
		File appDir = new File(System.getProperty("user.dir"), "");
		//File app = new File(appDir, "HomeBudget_3.2.5.ipa");
		File app = new File(appDir, "iXpenseIt_Expense_5.1.ipa");
		
		capabilities.setCapability("app",app.getAbsolutePath());
		this.driver = new IOSDriver<IOSElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		System.out.println("driver setup");
	}

	public void openApp(){
		System.out.println("Launching the app");
		this.driver.launchApp();
	}

	public void closeApp(){
		System.out.println("Closing the app");
		this.driver.closeApp();
	}
	
	public static void main(String[] args) throws MalformedURLException, InterruptedException {
		Test test = new Test();
		test.openApp();
		Thread.sleep(3000);
		test.closeApp();

	}
}